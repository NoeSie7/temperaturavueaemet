import Vue from 'vue';
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage';

Vue.use(MLInstaller)

export default new MLCreate({
  initial: 'English',
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('English').create({
      title: 'Hello {0}!',
      msg: 'You have {f} friends and {l} likes',
      textoprovincia: 'Province',
      img: require('../assets/flagofunitedkingdom_6362.png'),
      textomunicipio: 'Municipality',
      textotiempo: 'Weather',
      textotablafecha: 'Date'
    }),
    new MLanguage('Portugues').create({
      title: 'Oi {0}!',
      msg: 'Você tem {f} amigos e {l} curtidas',
      img: require('../assets/portugal-flag-31814.jpg'),
      textoprovincia: 'Província',
      textomunicipio: 'Município',
      textotiempo: 'Tempo',
      textotablafecha: 'Namorar'
    }),
    new MLanguage('Español').create({
      title: 'Hola {0}!',
      msg: 'Tu tienes {f} amigos y {l} curtidas',
      img: require('../assets/descarga.jpg'),
      textoprovincia: 'Provincia',
      textomunicipio: 'Municipio',
      textotiempo: 'Tiempo',
      textotablafecha: 'Fecha'
    }),
  ]
})
