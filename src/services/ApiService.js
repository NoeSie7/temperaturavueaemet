import axios from 'axios';

const searchProvince = function() {
   return axios.get('https://www.el-tiempo.net/api/json/v1/provincias')

};

const searchMunicipality = function(codigo) {
  console.log("ENTRO EN PROMISE MUNICIPIO", codigo);
  return axios.get(`https://www.el-tiempo.net/api/json/v1/provincias/${codigo}/municipios`);
};

const searchMunicipilatyWeather = function (codigo, codigopueblo) {
  return axios.get(`https://www.el-tiempo.net/api/json/v1/provincias/${codigo}/municipios/${codigopueblo}/weather`);
}

export {searchProvince, searchMunicipality, searchMunicipilatyWeather};

